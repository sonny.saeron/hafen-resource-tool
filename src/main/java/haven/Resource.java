package haven;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import haven.layers.*;

import java.util.*;

public class Resource {
    private static final Map<String, LayerFromBuf> bufcons = new HashMap<>();
    private static final Map<String, LayerFromJson> jsoncons = new HashMap<>();

    private Collection<Layer> layers = new LinkedList<>();
    public final String name;
    public final String in;
    public final String out;
    public int ver;

    public Resource(final String name, final String in, final String out) {
        this.name = name;
        this.in = in;
        this.out = out;
    }

    public interface Layer {
        String name();
        void save(final JsonObject root, final String out, final int id) throws Exception;
        void save(final MessageBuf data);
    }

    public interface LayerFromBuf {
        Layer cons(Resource res, Message buf) throws Exception;
    }

    public interface LayerFromJson {
        Layer cons(Resource res, JsonObject json) throws Exception;
    }

    static {
        bufcons.put(Image.NAME, Image::cons);
        jsoncons.put(Image.NAME, Image::cons);
        bufcons.put(Tooltip.NAME, Tooltip::cons);
        jsoncons.put(Tooltip.NAME, Tooltip::cons);
        bufcons.put(Pagina.NAME, Pagina::cons);
        jsoncons.put(Pagina.NAME, Pagina::cons);
        bufcons.put(WindowConfig.NAME, WindowConfig::cons);
        jsoncons.put(WindowConfig.NAME, WindowConfig::cons);
        bufcons.put(Midi.NAME, Midi::cons);
        jsoncons.put(Midi.NAME, Midi::cons);
        bufcons.put(Audio.NAME, Audio::cons);
        jsoncons.put(Audio.NAME, Audio::cons);
        bufcons.put(Anim.NAME, Anim::cons);
        jsoncons.put(Anim.NAME, Anim::cons);
        bufcons.put(Action.NAME, Action::cons);
        jsoncons.put(Action.NAME, Action::cons);
        bufcons.put(Neg.NAME, Neg::cons);
        jsoncons.put(Neg.NAME, Neg::cons);
    }

    private static final String SIG =  "Haven Resource 1";
    public void loadFromRes(final String fn) throws Exception {
        final MessageBuf in = new MessageBuf(Utils.readFile(fn));

        byte[] sig = SIG.getBytes(Utils.ascii);
        if(!Arrays.equals(sig, in.bytes(sig.length)))
            throw new RuntimeException("Invalid res signature for " + name);
        ver = in.uint16();
        final List<Layer> layers = new LinkedList<>();

        while(!in.eom()) {
            final String ln = in.string();
            final int len = in.int32();
            final byte[] data = in.bytes(len);
            final MessageBuf lin = new MessageBuf(data);
            final LayerFromBuf cons = bufcons.get(ln);
            if(cons != null) {
                layers.add(cons.cons(this, lin));
            } else {
                layers.add(Unknown.cons(this, ln, lin));
            }
        }
        this.layers = layers;
    }

    public void saveToRes(final MessageBuf buf) {
        buf.addbytes(SIG.getBytes(Utils.ascii));
        buf.adduint16(ver);
        for(final Layer l : layers) {
            buf.addstring(l.name());
            final MessageBuf data = new MessageBuf();
            l.save(data);
            final byte[] back = data.fin();
            buf.addint32(back.length);
            buf.addbytes(back);
        }
    }

    /**
     * JSON format:
     * {
     *    ver : [int32]
     *    layers : [
     *        [name] : {
     *            ...
     *        },
     *        ...
     *    ]
     * }
     */
    public void loadFromJSON(final JsonObject details) throws Exception {
        final List<Layer> layers = new LinkedList<>();
        ver = details.get("ver").getAsInt();
        final JsonArray root = (JsonArray)details.get("layers");
        for(final JsonElement ele : root) {
            final JsonObject data = ele.getAsJsonObject();
            final String ln = data.get("layer-name").getAsString();
            final LayerFromJson cons = jsoncons.get(ln);
            if(cons != null) {
                layers.add(cons.cons(this, data));
            } else {
                layers.add(Unknown.cons(this, ln, data));
            }
        }
        this.layers = layers;
    }

    public void savetoJSON(final JsonObject output) throws Exception {
        final Map<String, Integer> layerids = new HashMap<>();
        output.addProperty("ver", ver);
        final JsonArray layers = new JsonArray();
        output.add("layers", layers);
        for(final Layer l : this.layers) {
            final JsonObject layer = new JsonObject();
            layer.addProperty("layer-name", l.name());
            final int id = layerids.getOrDefault(l.name(), 0);
            layerids.put(l.name(), id+1);
            layers.add(layer);
            l.save(layer, out, id);
        }
    }

}
