/*
 *  This file is part of the Haven & Hearth game client.
 *  Copyright (C) 2009 Fredrik Tolf <fredrik@dolda2000.com>, and
 *                     Björn Johannessen <johannessen.bjorn@gmail.com>
 *
 *  Redistribution and/or modification of this file is subject to the
 *  terms of the GNU Lesser General Public License, version 3, as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  Other parts of this source tree adhere to other copying
 *  rights. Please see the file `COPYING' in the root directory of the
 *  source tree for details.
 *
 *  A copy the GNU Lesser General Public License is distributed along
 *  with the source tree of which this file is a part in the file
 *  `doc/LPGL-3'. If it is missing for any reason, please see the Free
 *  Software Foundation's website at <http://www.fsf.org/>, or write
 *  to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 *  Boston, MA 02111-1307 USA
 */

package haven;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Coord implements Comparable<Coord>, java.io.Serializable {
    private static final Pattern parse = Pattern.compile("Coord\\{\\s*([0-9]+)\\s*,\\s*([0-9]+)\\s*}");
    private static final String fmt = "Coord{%d, %d}";

    public final int x, y;

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coord(final String c) {
        final Matcher m = parse.matcher(c);
        if(m.find()) {
            x = Integer.parseInt(m.group(1));
            y = Integer.parseInt(m.group(2));
        } else {
            x = y = 0;
        }
    }

    public boolean equals(Object o) {
        if (!(o instanceof Coord))
            return (false);
        Coord c = (Coord) o;
        return ((c.x == x) && (c.y == y));
    }

    public int compareTo(Coord c) {
        if (c.y != y)
            return (c.y - y);
        if (c.x != x)
            return (c.x - x);
        return (0);
    }

    public int hashCode() {
        return (((y & 0xffff) * 31) + (x & 0xffff));
    }

    @Override
    public String toString() {
        return String.format(fmt, x, y);
    }
}
