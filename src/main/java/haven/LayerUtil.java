package haven;

import com.google.gson.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LayerUtil {
    private static final Logger logger = LogManager.getLogger(LayerUtil.class);

    public static List<Path> collect(final String root, final String type) throws IOException {
        final List<Path> paths = new ArrayList<>();
        Files.walkFileTree(new File(root).toPath(), new FileVisitor<>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs){
                if(file.toString().endsWith(type)) {
                    paths.add(file);
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
                return FileVisitResult.CONTINUE;
            }
        });
        return paths;
    }

    public static List<Path> concat(final List<Path> a, final List<Path> b) {
        a.addAll(b);
        return a;
    }

    public static void decode(final String root, final String output) throws IOException {
        final List<Resource> reses = new ArrayList<>();
        {
            //Collection files
            logger.atInfo().log("Collecting files");
            final List<Path> itms = concat(collect(root, ".res"), collect(root, ".cache"));

            for (final Path itm : itms) {
                final String fn = itm.getFileName().toString();
                final String name = fn.substring(0, fn.lastIndexOf('.'));
                final String out = output + itm.toString().substring(root.length(), itm.toString().lastIndexOf("."));
                reses.add(new Resource(name,  itm.toString(), out));
                logger.atInfo().log("Found resource [name {}] [from {}]", name, itm.toString());
            }
        }
        {
            //Load them
            logger.atInfo().log("Loading files");
            for(final Iterator<Resource> itr = reses.iterator(); itr.hasNext();) {
                final Resource res = itr.next();
                try {
                    logger.atInfo().log("Loading resource from res [name {}]", res.name);
                    res.loadFromRes(res.in);
                } catch (Exception e) {
                    System.out.println("Failed to load " + res.name + " from " + res.in);
                    e.printStackTrace();
                    itr.remove();
                }
            }
        }
        {
            //Save them
            logger.atInfo().log("Saving files");
            for(final Resource res : reses) {
                final JsonObject rootjson = new JsonObject();
                final Gson gson = new GsonBuilder().setPrettyPrinting().create();
                try {
                    logger.atInfo().log("Saving resource [name {}]", res.name);
                    res.savetoJSON(rootjson);
                    Utils.saveFile(res.out+".json", gson.toJson(rootjson).getBytes());
                } catch (Exception e) {
                    System.out.println("Failed to save " + res.name + " to " + res.out);
                    e.printStackTrace();
                }
            }
        }
    }

    public static void encode(final String root, final String output) throws IOException {
        final List<Resource> reses = new ArrayList<>();
        {
            //collect files
            logger.atInfo().log("Collecting files");
            final List<Path> itms = collect(root, ".json");

            for (final Path itm : itms) {
                final String fn = itm.getFileName().toString();
                final String name = fn.substring(0, fn.lastIndexOf('.'));
                final String out = output + itm.toString().substring(root.length(), itm.toString().lastIndexOf("."));
                reses.add(new Resource(name,  itm.toString(), out));
                logger.atInfo().log("Found resource [name {}] [from {}]", name, itm.toString());
            }
        }
        {
            //Load them
            logger.atInfo().log("Loading files");
            final Gson parser = new Gson();
            for(final Iterator<Resource> itr = reses.iterator(); itr.hasNext();) {
                final Resource res = itr.next();
                try {
                    final JsonObject rootjson = parser.fromJson(new FileReader(res.in), JsonObject.class);
                    logger.atInfo().log("Loading resource from JSON [name {}]", res.name);
                    res.loadFromJSON(rootjson);
                } catch (Exception e) {
                    System.out.println("Failed to load " + res.name + " from " + res.in);
                    e.printStackTrace();
                    itr.remove();
                }
            }
        }
        {
            //Save them
            logger.info("Saving files");
            for(final Resource res : reses) {
                final MessageBuf buf = new MessageBuf();
                try {
                    res.saveToRes(buf);
                    logger.atInfo().log("Saving resource [name {}]", res.name);
                    Utils.saveFile(res.out+".res", buf.fin());
                } catch (Exception e) {
                    System.out.println("Failed to save " + res.name + " to " + res.out);
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(final String[] args) throws IOException {

        // Tooltip for showing the usage
        if(args.length < 3) {
            System.err.println("Wrong usage!\njava -jar LayerUtil.jar -decode rootDirectory outputDirectory\njava -jar LayerUtil.jar -encode rootDirectory outputDirectory");
            return;
        }


        // If you are running the jar file from the absolute path it might be hard to pinpoint where does the app want to load the files from
        final Path path = new File(args[1]).toPath();
        logger.atInfo().log("Decoding files from: "+path.toAbsolutePath());

        switch (args[0]) {
            case "-encode":
                encode(args[1], args[2]);
                break;
            case "-decode":
                decode(args[1], args[2]);
                break;
        }
    }
}
