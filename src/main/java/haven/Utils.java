/*
 *  This file is part of the Haven & Hearth game client.
 *  Copyright (C) 2009 Fredrik Tolf <fredrik@dolda2000.com>, and
 *                     Björn Johannessen <johannessen.bjorn@gmail.com>
 *
 *  Redistribution and/or modification of this file is subject to the
 *  terms of the GNU Lesser General Public License, version 3, as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  Other parts of this source tree adhere to other copying
 *  rights. Please see the file `COPYING' in the root directory of the
 *  source tree for details.
 *
 *  A copy the GNU Lesser General Public License is distributed along
 *  with the source tree of which this file is a part in the file
 *  `doc/LPGL-3'. If it is missing for any reason, please see the Free
 *  Software Foundation's website at <http://www.fsf.org/>, or write
 *  to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 *  Boston, MA 02111-1307 USA
 */

package haven;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class Utils {
    public static final java.nio.charset.Charset utf8 = StandardCharsets.UTF_8;
    public static final java.nio.charset.Charset ascii = StandardCharsets.US_ASCII;

    public static int ub(byte b) {
        return (((int) b) & 0xff);
    }

    public static byte sb(int b) {
        return ((byte) b);
    }

    public static byte f2s8(float v) {
        return ((byte) Math.max(Math.min(Math.round(v * 127f), 127), -127));
    }

    public static byte f2u8(float v) {
        return ((byte) Math.max(Math.min(Math.round(v * 255f), 255), 0));
    }

    public static long uint32(int n) {
        return (n & 0xffffffffl);
    }

    public static int uint16d(byte[] buf, int off) {
        return (ub(buf[off]) | (ub(buf[off + 1]) << 8));
    }

    public static int int16d(byte[] buf, int off) {
        return (short) uint16d(buf, off);
    }

    public static long uint32d(byte[] buf, int off) {
        return ((long) ub(buf[off]) | ((long) ub(buf[off + 1]) << 8) | ((long) ub(buf[off + 2]) << 16) | ((long) ub(buf[off + 3]) << 24));
    }

    public static void uint32e(long num, byte[] buf, int off) {
        buf[off] = (byte) (num & 0xff);
        buf[off + 1] = (byte) ((num & 0x0000ff00) >> 8);
        buf[off + 2] = (byte) ((num & 0x00ff0000) >> 16);
        buf[off + 3] = (byte) ((num & 0xff000000) >> 24);
    }

    public static int int32d(byte[] buf, int off) {
        return ((int) uint32d(buf, off));
    }

    public static long int64d(byte[] buf, int off) {
        long b = 0;
        for (int i = 0; i < 8; i++)
            b |= ((long) ub(buf[off + i])) << (i * 8);
        return (b);
    }

    public static void int64e(long num, byte[] buf, int off) {
        for (int i = 0; i < 8; i++) {
            buf[off++] = (byte) (num & 0xff);
            num >>>= 8;
        }
    }

    public static void int32e(int num, byte[] buf, int off) {
        uint32e(((long) num) & 0xffffffff, buf, off);
    }

    public static void uint16e(int num, byte[] buf, int off) {
        buf[off] = sb(num & 0xff);
        buf[off + 1] = sb((num & 0xff00) >> 8);
    }

    public static void int16e(short num, byte[] buf, int off) {
        uint16e(((int) num) & 0xffff, buf, off);
    }

    public static String strd(byte[] buf, int[] off) {
        int i;
        for (i = off[0]; buf[i] != 0; i++) ;
        String ret;
        ret = new String(buf, off[0], i - off[0], StandardCharsets.UTF_8);
        off[0] = i + 1;
        return (ret);
    }

    public static double floatd(byte[] buf, int off) {
        int e = buf[off];
        long t = uint32d(buf, off + 1);
        int m = (int) (t & 0x7fffffffL);
        boolean s = (t & 0x80000000L) != 0;
        if (e == -128) {
            if (m == 0)
                return (0.0);
            throw (new RuntimeException("Invalid special float encoded (" + m + ")"));
        }
        double v = (((double) m) / 2147483648.0) + 1.0;
        if (s)
            v = -v;
        return (Math.pow(2.0, e) * v);
    }

    public static Coord cdec(Message buf) {
        return(new Coord(buf.int16(), buf.int16()));
    }

    public static float float32d(byte[] buf, int off) {
        return (Float.intBitsToFloat(int32d(buf, off)));
    }

    public static double float64d(byte[] buf, int off) {
        return (Double.longBitsToDouble(int64d(buf, off)));
    }

    public static void float32e(float num, byte[] buf, int off) {
        int32e(Float.floatToIntBits(num), buf, off);
    }

    public static void float64e(double num, byte[] buf, int off) {
        int64e(Double.doubleToLongBits(num), buf, off);
    }

    public static float hfdec(short bits) {
        int b = ((int) bits) & 0xffff;
        int e = (b & 0x7c00) >> 10;
        int m = b & 0x03ff;
        int ee;
        if (e == 0) {
            if (m == 0) {
                ee = 0;
            } else {
                int n = Integer.numberOfLeadingZeros(m) - 22;
                ee = (-15 - n) + 127;
                m = (m << (n + 1)) & 0x03ff;
            }
        } else if (e == 0x1f) {
            ee = 0xff;
        } else {
            ee = e - 15 + 127;
        }
        int f32 = ((b & 0x8000) << 16) |
                (ee << 23) |
                (m << 13);
        return (Float.intBitsToFloat(f32));
    }

    public static short hfenc(float f) {
        int b = Float.floatToIntBits(f);
        int e = (b & 0x7f800000) >> 23;
        int m = b & 0x007fffff;
        int ee;
        if (e == 0) {
            ee = 0;
            m = 0;
        } else if (e == 0xff) {
            ee = 0x1f;
        } else if (e < 127 - 14) {
            ee = 0;
            m = (m | 0x00800000) >> ((127 - 14) - e);
        } else if (e > 127 + 15) {
            return (((b & 0x80000000) == 0) ? ((short) 0x7c00) : ((short) 0xfc00));
        } else {
            ee = e - 127 + 15;
        }
        int f16 = ((b >> 16) & 0x8000) |
                (ee << 10) |
                (m >> 13);
        return ((short) f16);
    }

    public static float mfdec(byte bits) {
        int b = ((int) bits) & 0xff;
        int e = (b & 0x78) >> 3;
        int m = b & 0x07;
        int ee;
        if (e == 0) {
            if (m == 0) {
                ee = 0;
            } else {
                int n = Integer.numberOfLeadingZeros(m) - 29;
                ee = (-7 - n) + 127;
                m = (m << (n + 1)) & 0x07;
            }
        } else if (e == 0x0f) {
            ee = 0xff;
        } else {
            ee = e - 7 + 127;
        }
        int f32 = ((b & 0x80) << 24) |
                (ee << 23) |
                (m << 20);
        return (Float.intBitsToFloat(f32));
    }

    public static byte mfenc(float f) {
        int b = Float.floatToIntBits(f);
        int e = (b & 0x7f800000) >> 23;
        int m = b & 0x007fffff;
        int ee;
        if (e == 0) {
            ee = 0;
            m = 0;
        } else if (e == 0xff) {
            ee = 0x0f;
        } else if (e < 127 - 6) {
            ee = 0;
            m = (m | 0x00800000) >> ((127 - 6) - e);
        } else if (e > 127 + 7) {
            return (((b & 0x80000000) == 0) ? ((byte) 0x78) : ((byte) 0xf8));
        } else {
            ee = e - 127 + 7;
        }
        int f8 = ((b >> 24) & 0x80) |
                (ee << 3) |
                (m >> 20);
        return ((byte) f8);
    }

    public static int floordiv(int a, int b) {
        if (a < 0)
            return (((a + 1) / b) - 1);
        else
            return (a / b);
    }

    public static int floormod(int a, int b) {
        int r = a % b;
        if (r < 0)
            r += b;
        return (r);
    }

    public static int clip(int i, int min, int max) {
        if (i < min)
            return (min);
        if (i > max)
            return (max);
        return (i);
    }

    public static byte[] readFile(final String fn) throws Exception {
        final File file = new File(fn);
        final InputStream in = new FileInputStream(new File(fn));
        final byte[] dat = new byte[in.available()];
        int offset = 0;
        while(offset < dat.length) {
            final int ret = in.read(dat, offset, dat.length-offset);
            offset += ret;
            if(ret == -1)
                break;
        }
        return dat;
    }

    public static void saveFile(final String fn, final byte[] data) throws Exception {
        final File file = new File(fn);
        final File dir = new File(fn.substring(0, fn.lastIndexOf("\\")));
        dir.mkdirs();
        if(!file.exists())
            file.createNewFile();
        final OutputStream out = new FileOutputStream(new File(fn));
        out.write(data);
        out.flush();
        out.close();
    }
}
