package haven.layers;

import com.google.gson.JsonObject;
import haven.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * Json format
 *
 * {
 *     blah blah
 *     image : {
 *         z : [int16]
 *         subz : [int16]
 *         nooff : [uint8]
 *         id : [int16]
 *         o : [coord]
 *         img : [file-name-reference]
 *     }
 * }
 */
public class Image implements Resource.Layer {
    public static final String NAME = "image";
    public final int z, subz;
    public final int fl;
    public final int id;
    public final Float scale;
    public final String scalemethod;
    public final Coord o, tsz;
    public final byte[] data;

    public Image(final int z, final int subz, final int fl, final int id, final Coord o, final Coord tsz,
                 final Float scale, final String scalemethod, final byte[] data) {
        this.z = z;
        this.subz = subz;
        this.fl = fl;
        this.id = id;
        this.o = o;
        this.scale = scale;
        this.scalemethod = scalemethod;
        this.tsz = tsz;
        this.data = data;
    }

    public static Resource.Layer cons(Resource res, JsonObject json) throws Exception {
        final int z = json.get("z").getAsInt();
        final int subz = json.get("subz").getAsInt();
        final int fl = json.has("fl") ? json.get("fl").getAsInt() : 0;
        final int  id = json.get("id").getAsInt();
        final Coord o = new Coord(json.get("o").getAsString());
        final Coord tsz;
        if(json.has("tsz")) {
            tsz = new Coord(json.get("tsz").getAsString());
        } else {
            tsz = null;
        }
        Float scale;
        if(json.has("scale")) {
            scale = Float.parseFloat(json.get("scale").getAsString());
        } else {
            scale = null;
        }
        final String scalemethod;
        if(json.has("scale-method")) {
            scalemethod = json.get("scale-method").getAsString();
        } else {
            scalemethod = null;
        }
        final byte[] data = Utils.readFile(json.get("img").getAsString());

        return new Image(z, subz, fl, id, o, tsz, scale, scalemethod, data);
    }

    public static Resource.Layer cons(Resource res, Message buf) {
        final int z = buf.int16();
        final int subz = buf.int16();
        int fl = buf.uint8();
        final int  id = buf.int16();
        final Coord o = Utils.cdec(buf);
        Coord tsz = null;
        Float scale = null;
        String scalemethod = null;
        if((fl & 4) != 0) { // kvdata not supported
            while(true) {
                String key = buf.string();
                if(key.equals(""))
                    break;
                int len = buf.uint8();
                if((len & 0x80) != 0)
                    len = buf.int32();
                byte[] data = buf.bytes(len);
                Message val = new MessageBuf(data);
                if(key.equals("tsz")) {
                    tsz = val.coord();
                } else if(key.equals("scale")) {
                    scale = val.float32();
                } else if(key.equals("scale-method")) {
                    scalemethod = val.string();
                }
            }
        }

        final byte[] data = buf.bytes();

        return new Image(z, subz, fl, id, o, tsz, scale, scalemethod, data);
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public void save(MessageBuf data) {
        data.addint16((short)z);
        data.addint16((short)subz);
        data.adduint8(fl);
        data.addint16((short)id);
        data.addcoord16(o);
        if(scale != null || tsz != null) {
            if(scale != null) {
                data.addstring("scale");
                data.adduint8(4);
                data.addfloat32(scale);
            }
            if(tsz != null) {
                data.addstring("scale");
                data.adduint8(8);
                data.addcoord(tsz);
            }
            if(scalemethod != null) {
                data.addstring("scale-method");
                data.adduint8(scalemethod.length()+1);
                data.addstring(scalemethod);
            }
            data.adduint8(0);
        }
        data.addbytes(this.data);
    }

    @Override
    public void save(JsonObject root, String out, int id) throws Exception {
        final String fn = out + "\\image-"+id+".png";
        root.addProperty("z", z);
        root.addProperty("subz", subz);
        root.addProperty("fl", fl);
        root.addProperty("id", id);
        root.addProperty("o", o.toString());
        root.addProperty("img", fn);
        if(scale != null)
            root.addProperty("scale", scale);
        if(tsz != null)
            root.addProperty("tsz", tsz.toString());
        if(scalemethod != null)
            root.addProperty("scale-method", scalemethod);
        Utils.saveFile(fn, data);
    }
}
