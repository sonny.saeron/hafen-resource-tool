package haven.layers;

import com.google.gson.JsonObject;
import haven.Message;
import haven.MessageBuf;
import haven.Resource;
import haven.Utils;

/**
 *
 * Json format
 * {
 *     blah blah
 *     pagina : {
 *         text : [value]
 *     }
 * }
 *
 */
public class Pagina implements Resource.Layer {
    public static final String NAME = "pagina";
    public final String text;

    public Pagina(final String t) {
        this.text = t;
    }

    public static Resource.Layer cons(Resource res, Message buf) {
        return new Tooltip(new String(buf.bytes(), Utils.utf8));
    }

    public static Resource.Layer cons(Resource res, JsonObject json) {
        return new Tooltip(json.get("text").getAsString());
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public void save(MessageBuf data) {
        data.addstring(text);
    }

    @Override
    public void save(JsonObject root, String out, int id)  {
        root.addProperty("text", text);
    }
}
