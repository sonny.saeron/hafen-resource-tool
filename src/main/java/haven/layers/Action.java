package haven.layers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import haven.Message;
import haven.MessageBuf;
import haven.Resource;

public class Action implements Resource.Layer {
    public static final String NAME = "action";
    public final String parent;
    public final int parentver;
    public final String name;
    public final String preq;
    public final char hk;
    public final String[] ad;

    public Action(final JsonObject json) {
        parent = json.get("parent").getAsString();
        parentver = json.get("parentver").getAsInt();
        name =  json.get("name").getAsString();
        preq = json.get("preq").getAsString();
        hk = json.get("hk").getAsString().charAt(0);
        final var arr = json.get("ad").getAsJsonArray();
        ad = new String[arr.size()];
        for(int i = 0; i < ad.length; ++i) {
            ad[i] = arr.get(i).getAsString();
        }
    }

    public Action(final Message buf) {
        parent = buf.string();
        parentver = buf.uint16();
        name = buf.string();
        preq = buf.string(); /* Prerequisite skill */
        hk = (char)buf.uint16();
        ad = new String[buf.uint16()];
        for(int i = 0; i < ad.length; i++)
            ad[i] = buf.string();
    }

    public static Resource.Layer cons(Resource res, Message buf) {
        return new Action(buf);
    }

    public static Resource.Layer cons(Resource res, JsonObject json) {
        return new Action(json);
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public void save(MessageBuf data) {
        data.addstring(parent);
        data.adduint16(parentver);
        data.addstring(name);
        data.addstring(preq);
        data.adduint16(hk);
        data.adduint16(ad.length);
        for(final var a : ad)
            data.addstring(a);
    }

    @Override
    public void save(JsonObject root, String out, int id)  {
        root.addProperty("parent", parent);
        root.addProperty("parentver", parentver);
        root.addProperty("name", name);
        root.addProperty("preq", preq);
        root.addProperty("hk", hk+"");
        final var arr = new JsonArray();
        for(final var a : ad)
            arr.add(a);
        root.add("ad", arr);
    }
}
