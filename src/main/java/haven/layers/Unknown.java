package haven.layers;

import com.google.gson.JsonObject;
import haven.Message;
import haven.MessageBuf;
import haven.Resource;
import haven.Utils;


/**
 *
 * Json Format:
 * {
 *     blah blah
 *
 *     [name] : {
 *         data : [data]
 *     }
 * }
 */
public class Unknown implements Resource.Layer {
    public final String name;
    public final byte[] data;

    public Unknown(final String name, final byte[] data) {
        this.name = name;
        this.data = data;
    }

    public static Resource.Layer cons(Resource res, String name, Message buf) {
        return new Unknown(name, buf.bytes());
    }

    public static Resource.Layer cons(Resource res, String name, JsonObject json) throws Exception {
        return new Unknown(name, Utils.readFile(json.get("data").getAsString()));
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public void save(MessageBuf data) {
        data.addbytes(this.data);
    }

    @Override
    public void save(JsonObject root, final String out, final int id) throws Exception {
        final String fn = out+"\\"+name+"-"+id+".dat";
        Utils.saveFile(fn, data);
        root.addProperty("data", fn);
    }
}
