package haven.layers;

import com.google.gson.JsonObject;
import haven.Message;
import haven.MessageBuf;
import haven.Resource;
import haven.Utils;

public class Midi implements Resource.Layer {
    public static final String NAME = "midi";
    public final byte[] data;

    public Midi(final byte[] data) {
        this.data = data;
    }

    public static Resource.Layer cons(Resource res, Message buf) {
        return new Midi(buf.bytes());
    }

    public static Resource.Layer cons(Resource res, JsonObject json) throws Exception {
        return new Midi(Utils.readFile(json.get("midi").getAsString()));
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public void save(MessageBuf data) {
        data.addbytes(this.data);
    }

    @Override
    public void save(JsonObject root, final String out, final int id) throws Exception {
        final String fn = out+"\\midi-"+id+".midi";
        Utils.saveFile(fn, data);
        root.addProperty("midi", fn);
    }
}
