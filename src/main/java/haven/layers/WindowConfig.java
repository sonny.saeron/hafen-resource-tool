package haven.layers;

import com.google.gson.JsonObject;
import haven.*;

public class WindowConfig implements Resource.Layer {
    public static final String NAME = "windowconfig";
    final Coord tlc;
    final Coord brc;
    final Coord capc;
    final Coord btnc;

    public WindowConfig(final Coord tlc, final Coord brc, final Coord capc, final Coord btnc) {
        this.tlc = tlc;
        this.brc = brc;
        this.capc = capc;
        this.btnc = btnc;
    }

    public static Resource.Layer cons(Resource res, Message buf) {
        return new WindowConfig(Utils.cdec(buf), Utils.cdec(buf),
                Utils.cdec(buf), Utils.cdec(buf));
    }

    public static Resource.Layer cons(Resource res, JsonObject json) {
        return new WindowConfig(new Coord(json.get("tlc").getAsString()),
                new Coord(json.get("brc").getAsString()),
                new Coord(json.get("capc").getAsString()),
                new Coord(json.get("btnc").getAsString()));
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public void save(MessageBuf data) {
        data.addcoord16(tlc);
        data.addcoord16(brc);
        data.addcoord16(capc);
        data.addcoord16(btnc);
    }

    @Override
    public void save(JsonObject root, String out, int id)  {
        root.addProperty("tlc", tlc.toString());
        root.addProperty("brc", brc.toString());
        root.addProperty("capc", capc.toString());
        root.addProperty("btnc", btnc.toString());
    }
}
