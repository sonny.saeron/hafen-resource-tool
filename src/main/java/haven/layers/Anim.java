package haven.layers;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import haven.Message;
import haven.MessageBuf;
import haven.Resource;

public class Anim implements Resource.Layer {
    public static final String NAME = "anim";
    private final int[] ids;
    private final int id, d;

    public Anim(final int id, final int d, final int[] ids) {
        this.id = id;
        this.d = d;
        this.ids = ids;
    }

    public static Resource.Layer cons(Resource res, Message buf) {
        final var id = buf.int16();
        final var d = buf.uint16();
        final var ids = new int[buf.uint16()];
        for(int i = 0; i < ids.length; i++)
            ids[i] = buf.int16();
        return new Anim(id, d, ids);
    }

    public static Resource.Layer cons(Resource res, JsonObject json) throws Exception {
        final var id = json.get("id").getAsInt();
        final var d = json.get("d").getAsInt();
        final var arr = json.get("ids").getAsJsonArray();
        final var ids = new int[arr.size()];
        for(int i = 0; i < ids.length; ++i) {
            ids[i] = arr.get(i).getAsInt();
        }
        return new Anim(id, d, ids);
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public void save(MessageBuf data) {
        data.addint16((short)id);
        data.adduint16(d);
        data.adduint16(ids.length);
        for(final var id : ids)
            data.addint16((short)id);
    }

    @Override
    public void save(JsonObject root, final String out, final int id) throws Exception {
        root.addProperty("id", id);
        root.addProperty("d", d);
        final var arr = new JsonArray();
        for(final var sid : ids)
            arr.add(sid);
        root.add("ids", arr);
    }
}
