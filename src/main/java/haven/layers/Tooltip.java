package haven.layers;

import com.google.gson.JsonObject;
import haven.Message;
import haven.MessageBuf;
import haven.Resource;
import haven.Utils;

/**
 *
 * Json format
 * {
 *     blah blah
 *     tooltip : {
 *         t : [value]
 *     }
 * }
 *
 */
public class Tooltip implements Resource.Layer {
    public static final String NAME = "tooltip";
    public final String t;

    public Tooltip(final String t) {
        this.t = t;
    }

    public static Resource.Layer cons(Resource res, Message buf) {
        return new Tooltip(new String(buf.bytes(), Utils.utf8));
    }

    public static Resource.Layer cons(Resource res, JsonObject json) {
        return new Tooltip(json.get("t").getAsString());
    }


    @Override
    public String name() {
        return NAME;
    }

    @Override
    public void save(MessageBuf data) {
        data.addstring(t);
    }

    @Override
    public void save(JsonObject root, String out, int id)  {
        root.addProperty("t", t);
    }
}
