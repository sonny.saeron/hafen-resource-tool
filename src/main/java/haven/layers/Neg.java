package haven.layers;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import haven.Coord;
import haven.Message;
import haven.MessageBuf;
import haven.Resource;

public class Neg implements Resource.Layer {
    public static final String NAME = "neg";
    public final Coord cc;
    public final Coord bc, bs, sz;
    public final Coord[][] ep;

    public Neg(final Coord cc, final Coord bc, final Coord bs, final Coord sz,
               final Coord[][] ep) {
        this.cc = cc;
        this.bc = bc;
        this.bs = bs;
        this.sz = sz;
        this.ep = ep;
    }

    public static Resource.Layer cons(Resource res, Message buf) {
        final var cc = buf.coord16();
        final var bc = buf.coord16();
        final var bs = buf.coord16();
        final var sz = buf.coord16();
        final var en = buf.uint8();
        final var ep = new Coord[en][0];
        for(var i = 0; i < en; ++i) {
            final var epid = buf.uint8();
            final var cn = buf.uint16();
            ep[epid] = new Coord[cn];
            for(var o = 0; o < cn; ++o) {
                ep[epid][o] = buf.coord16();
            }
        }
        return new Neg(cc, bc, bs, sz, ep);
    }

    public static Resource.Layer cons(Resource res, JsonObject json) {
        final var cc = new Coord(json.get("cc").getAsString());
        final var bc = new Coord(json.get("bc").getAsString());
        final var bs = new Coord(json.get("bs").getAsString());
        final var sz = new Coord(json.get("sz").getAsString());
        final var eparr = json.get("ep").getAsJsonArray();
        final var ep = new Coord[eparr.size()][];
        for(var i = 0; i < ep.length; ++i) {
            final var epiarr = eparr.get(i).getAsJsonArray();
            ep[i] = new Coord[epiarr.size()];
            for(var o = 0; o < ep[i].length; ++o) {
                ep[i][o] = new Coord(epiarr.get(o).getAsString());
            }
        }
        return new Neg(cc, bc, bs, sz, ep);
    }


    @Override
    public String name() {
        return NAME;
    }

    @Override
    public void save(MessageBuf data) {
        data.addcoord16(cc);
        data.addcoord16(bc);
        data.addcoord16(bs);
        data.addcoord16(sz);
        data.adduint8(ep.length);
        for(var i = 0; i < ep.length; ++i) {
            data.adduint8(i);
            data.adduint16(ep[i].length);
            for(var o = 0; o < ep[i].length; ++o) {
                data.addcoord16(ep[i][o]);
            }
        }
    }

    @Override
    public void save(JsonObject root, String out, int id)  {
        root.addProperty("cc", cc.toString());
        root.addProperty("bc", bc.toString());
        root.addProperty("bs", bs.toString());
        root.addProperty("sz", sz.toString());
        final var eparr = new JsonArray();
        for (Coord[] coords : ep) {
            final var arr = new JsonArray();
            for (Coord coord : coords) {
                arr.add(coord.toString());
            }
            eparr.add(arr);
        }
        root.add("ep", eparr);
    }
}
