package haven.layers;

import com.google.gson.JsonObject;
import haven.Message;
import haven.MessageBuf;
import haven.Resource;
import haven.Utils;

public class Audio implements Resource.Layer {
    public static final String NAME = "audio";
    public final byte[] data;

    public Audio(final byte[] data) {
        this.data = data;
    }

    public static Resource.Layer cons(Resource res, Message buf) {
        return new Audio(buf.bytes());
    }

    public static Resource.Layer cons(Resource res, JsonObject json) throws Exception {
        return new Audio(Utils.readFile(json.get("audio").getAsString()));
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public void save(MessageBuf data) {
        data.addbytes(this.data);
    }

    @Override
    public void save(JsonObject root, final String out, final int id) throws Exception {
        final String fn = out+"\\audio-"+id+".ogg";
        Utils.saveFile(fn, data);
        root.addProperty("audio", fn);
    }
}
